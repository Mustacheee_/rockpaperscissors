package com.mustacheee.roskpaperscissors;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mustacheee.rockpaperscissors.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class HighScoresFragment extends Fragment {

    public HighScoresFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_high_scores, container, false);
    }
}
