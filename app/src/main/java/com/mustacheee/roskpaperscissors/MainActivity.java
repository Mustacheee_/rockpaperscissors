package com.mustacheee.roskpaperscissors;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.mustacheee.rockpaperscissors.R;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new MainFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class MainFragment extends Fragment {
		Button rockBtn, paperBtn, scissorsBtn, clearBtn;
		TextView resultsTextView, winStreakTextView, highScoreTextView;
		ImageView playersChoice, computersChoice;
		Drawable rock, paper, scissors;
		
		SharedPreferences preferences;
		
		private ResultGenerator r = new ResultGenerator();
		
		private final int ROCK = 0;
		private final int PAPER = 1;
		private final int SCISSORS = 2;
		
		public int winStreak;
		public int highScore;
		
		private int pPick, cPick;
		String resultsText = "";
		
		
		public MainFragment() {
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState){
			super.onCreate(savedInstanceState);

			if(savedInstanceState != null){
				pPick = savedInstanceState.getInt("pPick");
				cPick = savedInstanceState.getInt("cPick");
				resultsText = savedInstanceState.getString("resultsText");
			}
			else{
				pPick = -1;
				cPick = -1;
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);


			AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
			AdRequest adRequest = new AdRequest.Builder().addTestDevice("059b513e4b194b1f").build();
			mAdView.loadAd(adRequest);

			this.rock = getResources().getDrawable(R.drawable.rock);
			this.paper = getResources().getDrawable(R.drawable.paper);
			this.scissors = getResources().getDrawable(R.drawable.scissors);
			
			this.rockBtn = (Button) rootView.findViewById(R.id.rockBtn);
			this.paperBtn = (Button) rootView.findViewById(R.id.paperBtn);
			this.scissorsBtn = (Button) rootView.findViewById(R.id.scissorsBtn);
			this.clearBtn = (Button) rootView.findViewById(R.id.clearBtn);
			
			this.resultsTextView = (TextView) rootView.findViewById(R.id.resultsTextView);
			this.winStreakTextView = (TextView) rootView.findViewById(R.id.winStreakTextView);
			this.highScoreTextView = (TextView) rootView.findViewById(R.id.highScoreTextView);
			
			this.playersChoice = (ImageView) rootView.findViewById(R.id.playerChoiceImage);
			this.computersChoice = (ImageView) rootView.findViewById(R.id.computerChoiceImage);
			
			this.preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
			
			this.winStreak = preferences.getInt("winStreak", 0);
			this.highScore = preferences.getInt("highScore", 0);
			
			winStreakTextView.setText("Current Win Streak: " + winStreak);
			highScoreTextView.setText("HighScore: " + highScore);
			
			resultsTextView.setText(resultsText);
			resultsTextView.setMovementMethod(new ScrollingMovementMethod());
			
			if(cPick != -1){
				if(cPick == ROCK)
					computersChoice.setImageDrawable(rock);
				else if (cPick == PAPER)
					computersChoice.setImageDrawable(paper);
				else if (cPick == SCISSORS)
					computersChoice.setImageDrawable(scissors);
			}
			
			if(pPick != -1){
				if(pPick == ROCK)
					playersChoice.setImageDrawable(rock);
				else if (pPick == PAPER)
					playersChoice.setImageDrawable(paper);
				else if (pPick == SCISSORS)
					playersChoice.setImageDrawable(scissors);
			}
			
			rockBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					selectRock();
				}
			});
			
			paperBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					selectPaper();
				}
			});
			
			scissorsBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					selectScissors();
				}
			});
			
			clearBtn.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View v){
					resultsTextView.setText("");
					resultsTextView.scrollTo(0, 0);
				}
			});
			return rootView;
		}
		
		private void selectRock(){
			pPick = ROCK;
			playersChoice.setImageDrawable(rock);
			
			int result = r.getNext();
			
			if(result == ROCK){
				cPick = ROCK;
				computersChoice.setImageDrawable(rock);
				displayTie("ROCK");
				return;
			}
			
			else if (result == PAPER){
				cPick = PAPER;
				computersChoice.setImageDrawable(paper);
				displayLoss("ROCK", "PAPER");
				return;
			}
			
			else {
				cPick = SCISSORS;
				computersChoice.setImageDrawable(scissors);
				displayWin("ROCK", "SCISSORS");
			}
		}

		private void selectPaper(){
			pPick = PAPER;
			playersChoice.setImageDrawable(paper);
			
			int result = r.getNext();
			
			if(result == PAPER){
				cPick = PAPER;
				computersChoice.setImageDrawable(paper);
				displayTie("PAPER");
				return;
			} else if(result == SCISSORS){
				cPick = SCISSORS;
				computersChoice.setImageDrawable(scissors);
				displayLoss("PAPER", "SCISSORS");
				return;
			}
			else{
				cPick = ROCK;
				computersChoice.setImageDrawable(rock);
				displayWin("PAPER", "ROCK");
			}
		}

		private void selectScissors(){
			pPick = SCISSORS;
			playersChoice.setImageDrawable(scissors);
			
			int result = r.getNext();
			
			if(result == SCISSORS){
				cPick = SCISSORS;
				computersChoice.setImageDrawable(scissors);
				displayTie("SCISSORS");
				return;
			} else if (result == ROCK){
				cPick = ROCK;
				computersChoice.setImageDrawable(rock);
				displayLoss("SCISSORS", "ROCK");
				return;
			}
			else{
				cPick = PAPER;
				computersChoice.setImageDrawable(paper);
				displayWin("SCISSORS", "PAPER");
			}

		}
		
		private void displayTie(String choice){
			String s = "It was a tie! Both players chose " + choice + '\n';
			resultsTextView.append(s + "\n");
		}
		
		private void displayWin(String pChoice, String cChoice){
			String s = "You win! You chose " + pChoice + " and the computer chose " + cChoice + '\n';
			winStreak++;
			updateWinStreak();
			updateHighScore();
			resultsTextView.append(s + "\n");
		}
		
		private void displayLoss(String pChoice, String cChoice){
			String s = "You Lost! You chose " + pChoice + " and the computer chose " + cChoice + '\n';
			updateHighScore();
			clearWinStreak();
			updateWinStreak();
			resultsTextView.append(s + "\n");
		}
		
		private void updateHighScore(){
			Editor editor = preferences.edit();
			if(winStreak <= highScore){
				return;
			}
			
			else{
				highScoreTextView.setText("HighScore: "+ winStreak);
				editor.putInt("highScore", winStreak);
				editor.commit();
				return;
			}
		}
		
		private void updateWinStreak(){
			Editor editor = preferences.edit();
			editor.putInt("winStreak", winStreak);
			editor.commit();
			winStreakTextView.setText("Current Win Streak: " + winStreak);
		}

		private void clearWinStreak(){
			winStreak = 0;
			Editor editor = preferences.edit();
			editor.putInt("winStreak", winStreak);
			editor.commit();
		}
		
		@Override
		public void onPause(){
			super.onPause();
			Editor editor = preferences.edit();
			editor.putInt("winStreak", winStreak);
			
			if(winStreak > highScore)
				editor.putInt("highScore", winStreak);
			
			editor.commit();
		}
		
		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			savedInstanceState.putString("resultsText", resultsTextView.getText().toString());
			savedInstanceState.putInt("pPick", pPick);
			savedInstanceState.putInt("cPick", cPick);
			
			super.onSaveInstanceState(savedInstanceState);
		}
	}
	
}
