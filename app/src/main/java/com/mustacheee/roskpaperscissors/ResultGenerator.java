package com.mustacheee.roskpaperscissors;

import java.util.Random;

public class ResultGenerator {
	Random r;
	
	public ResultGenerator(){
		this.r = new Random();
	}
	
	public int getNext(){
		return r.nextInt() % 3;
	}
	
}
